package com.ab;

import java.io.File;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ab.controller.FileUploadController;

@SpringBootApplication
public class BootFileUploadApplication {

	public static void main(String[] args) {
		new File(FileUploadController.uploadDirectory).mkdir();
		SpringApplication.run(BootFileUploadApplication.class, args);
	}

}
